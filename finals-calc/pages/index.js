import Head from 'next/head';
import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { createTheme, ThemeProvider } from '@mui/material/styles';

const ColorModeContext = React.createContext({ toggleColorMode: () => {} });

function ValidateForm() {
  var x;
  var y;
  // var z = 70;
  let RFG = 0;
  
  x = document.getElementById("Q").value;
  y = document.getElementById("QQ").value;
  // z = document.getElementById("F").value;
  
  if (x == 0) {
    document.getElementById("answer").innerText = "Input Numbers Please";
  } else if (y == 0) {
    document.getElementById("answer").innerText = "Input Numbers Please";
  } else {
    RFG = 350-2*x-2*y
    if (RFG < 0) {
      document.getElementById("answer").innerText = "Less than 0"
    } else if (RFG > 100) {
      document.getElementById("answer").innerText = "Over 100% (Impossible?)"
    } else {
    document.getElementById("answer").innerText = RFG + "%";
  }}}

export default function Home() {
  const [mode, setMode] = React.useState('dark');
  const colorMode = React.useMemo(
    () => ({
      toggleColorMode: () => {
        setMode((prevMode) => (prevMode === 'light' ? 'dark' : 'light'));
        document.getElementsByTagName("body")[0].classList.toggle("light");
      },
    }),
    [],
  );
  const theme = React.useMemo(
    () =>
      createTheme({
        palette: {
          mode,
        },
      }),
    [mode],
  );
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <Head>
          <title>Finals Calculator</title>
          <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
          <meta name="description" content="A simple calculator to find out what grade you need on your final to pass the class"></meta>
          <link rel='manifest' href='/manifest.webmanifest' />
        </Head>
        <main className='main'>
          <div>
            <div className="themeSwitcher">
              <Button variant="outlined" onClick={colorMode.toggleColorMode}>{theme.palette.mode === 'light' ? 'dark' : 'light'}</Button>
              {/* <Button variant="outlined" onClick={() => {colorMode.toggleColorMode("dark")}}>Dark</Button> */}
              <br />
            </div>
            {/* <p hidden>wanted grade in class</p> */ }
            <div>
              <TextField 
                id="Q" 
                label="Q1 Grade" 
                variant="standard" 
                type={"Number"} 
                onInput={ValidateForm} 
                className="TextField"
                />
              <br />
              <TextField 
                id="QQ" 
                label="Q2 Grade" 
                variant="standard" 
                type={"Number"} 
                onInput={ValidateForm} 
                className="TextField"
                />
            </div>
            <div>
              <br/>
              <Button onClick={ValidateForm} variant="outlined" className="padTop">Submit</Button> 
            </div>
          </div>
          <br /> <p>Final grade needed to pass the class: </p>
          <h1 id="answer"></h1>
        </main>
        <footer>
          <a href="https://04d0649d.finals.pages.dev/" className='link'>You can also pick your final grade to get your course average here</a>!
        </footer>
      </ThemeProvider>
    </ColorModeContext.Provider>
  )
}
